@extends('layouts.app')

@section('content')
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your History</div>

                <div class="panel-body">
                  @if(count($deposits) > 0)
                  <h4> Your deposits</h4>
                  @foreach($deposits as $deposit)
                    <table class="table"> 
                      <tbody>
                        <tr><td>You deposit <span class="amount">{{$deposit->amount}}</span> LE at {{$deposit->created_at->toDayDateTimeString()}}</td><td></td></tr>
                        </tbody>
                    </table>
                  @endforeach
                  @else
                    <h4> No deposits yet ...</h4>
                  @endif

                   @if(count($withdraws) > 0)
                   <h4> Your withdraws</h4>
                    @foreach($withdraws as $withdraw)
                      <table class="table"> 
                        <tbody>
                          <tr><td>You withdraw <span class="amount">{{$withdraw->amount}}</span> LE at {{$withdraw->created_at->toDayDateTimeString()}}</td><td></td></tr>
                          </tbody>
                      </table>
                    @endforeach
                    @else
                      <h4> No withdraws yet ...</h4>
                    @endif

                    @if(count($transactions) > 0)
                   <h4> Your transactions</h4>
                    @foreach($transactions as $transaction)
                      <table class="table"> 
                        <tbody>
                          <tr><td>You transfer <span class="amount">{{$transaction->amount}}</span> LE at {{$transaction->created_at->toDayDateTimeString()}} to <span class="amount">{{$transaction->to}}</span></td></tr>
                          </tbody>
                      </table>
                    @endforeach
                    @else
                      <h4> No transactions yet ...</h4>
                    @endif

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
