@extends('layouts.app')
@section('content')

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif          
        </div>
         <div class="intro-header">
        <div class="container new">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>My Wallet</h1>
                        <h3></h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                             <!-- Authentication Links -->
                        
                            <li><a href="{{ route('login') }}" class="btn btn-default btn-lg"><i class="fa fa-sign-in"></i> Login </a></li>
                            <li><a href="{{ route('register') }}" class="btn btn-default btn-lg" ><i class="fa fa-lock"></i> Register </a></li>
                        
                                    
                                </ul>
                            </li>
                        
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
@endsection 