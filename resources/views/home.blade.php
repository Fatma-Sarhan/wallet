@extends('layouts.app')

@section('content')
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   <h4> Welcome {{Auth::user()->name}}  </h4>

                   <h5> your current balance at your wallet = {{Auth::user()->balance}} LE</h5> 
                   <br>
                   <h5>Which process you want to do ? </h5>
                   <ul>
                        <li><a href="/deposit" > Deposit </a> </li>
                        <li><a href="/withdraw" > Withdraw </a> </li>
                        <li><a href="/transfer" > Transfer </a> </li>
                   </ul>
                    <div class="history">
                        <a href="/history" >YourHistory</a>
                    </div>
                     <div class="api">
                        <a href="/api/listuser?api_token={{Auth::user()->api_token}}">Test Api function </a>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
