@extends('layouts.app')

@section('content')
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">withdraw process</div>

                <div class="panel-body">
                  <h5> your current balance at your wallet = <span class="amount" id="balance">{{Auth::user()->balance}}  </span>   LE</h5> 
                  <p>New Withdraw :</p>
    
                      <form class="form-inline" action="/withdraw" method="POST">
                      {{ csrf_field() }}
                      <input type="number" class="form-control mb-2 mr-sm-2 mb-sm-0"  placeholder="Enter money amount" name="amount" id="amount" min="0" max="{{Auth::user()->balance}}">
                       
                      <button type="submit" class="btn btn-primary withdraw" data-rowtok="{{ csrf_token() }}">Withdraw</button>
                    </form>
                     <br>
                    <div id="msg" class="hide"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
