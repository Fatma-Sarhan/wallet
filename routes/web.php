<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

	Route::get('/home', 'HomeController@index');
	//Deposit routes...
	Route::get('/deposit', 'UserController@deposit');
	Route::post('/deposit','UserController@newdeposit');
	//Withdraw routes...
	Route::get('/withdraw', 'UserController@withdraw');
	Route::post('/withdraw','UserController@newwithdraw');
	//Transfer routes...
	Route::get('/transfer', 'TransferController@transfer');
	Route::post('/transfer', 'TransferController@newtransfer');
	//History 
	Route::get('/history', 'UserController@history');
});

//Error pages...

Route::get('notfound', function () {
    return view('404');
});


