<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Deposit;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'balance','api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /* 1:M Relation user:deposits */
    public function deposits()
    {
        return $this->hasMany('App\Deposit');
    }
    /* 1:M Relation user:withdraws */
    public function withdraws()
    {
        return $this->hasMany('App\Withdraw');
    }
    /* 1:M Relation user:transactions */
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}
