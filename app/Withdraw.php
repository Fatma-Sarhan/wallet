<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = [ 'amount', 'user_id'];

    /* 1:M Relation user:withdraws */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
