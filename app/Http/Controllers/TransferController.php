<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Process;
use App\User;
use Auth;
use Mail;
use Carbon\Carbon;
use App\Transaction;

class TransferController extends Controller
{
    public function transfer()
    {
    	return view('users.transfer');
    }
    
    public function newtransfer(Request $request)
    {
    	$amount = $request->amount;
    	$email =  $request-> email;
    	//Sender user...
    	$id = Auth::user()->id;
    	$user = User::findOrfail($id);
    	$send_email = $user->email;

    	if($email != $send_email)
    	{
    		if($amount <= 0 || $amount > $user->balance )
    		{
    			$error = "Not allowed value .... ";
    			return Response::json(['st'=>'novalue','message'=>$error]);
    		}
    		else
    		{
    			$new_balance = $user->balance - $amount;
		    	$user->balance = $new_balance;
		    	$user->update();
		    	//Received user...
		        $received_user = User::where('email',$email)->get()->first();
		       
		        if(count($received_user)>0)
		        {
		        	$new_balance = $received_user->balance + $amount;
		        	$received_user->balance = $new_balance;
			        $received_user->update();
			        //New Transaction...
			    	$time =  Carbon::now();
			    	Transaction::create([
			            'amount' => $amount,
			            'created_at' =>$time,
			            'user_id' => $id,
			            'to'=>$email,
			        ]);

			        //Retrive data for email
			        $name = $received_user->name;
			        //Sending mail...
			    	Mail::send('emails.send', ['name' => $name ,'amount'=>$amount, 'email'=>$email], function ($message) use ($email)
			        {
			            $message->from(Auth::user()->email);

			            $message->to($email);

			            $message->subject('Transaction process');

			        });
		    	
		    	//Response for Ajax function...
		        return Response::json(['st'=>'true','amount'=>$new_balance]);

	        }
	        else
	        {
	        	$error = "This user not in our system ....";
    			return Response::json(['st'=>'false','message'=>$error]);
    			// return $error;

	        }
	        


    		}
    		
    	}
    	else
    	{
    		$error = "Not allowed process ....";
    		return Response::json(['st'=>'not','message'=>$error]);
    	}
    	
    	
    }

}
