<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Response;
use Auth;
use DB;

class ApiController extends Controller
{
    //Api function...
    public function users()
    {
    	//Get all users objects...
    	// $users = DB::table('users')->get();
    	// return Response::json($users);

    	//Get only username and balance...
    	$names = DB::table('users')->pluck('name');
    	$balances = DB::table('users')->pluck('balance');

    	return Response::json(['names'=>$names ,'balances'=>$balances]);
    	
    }

}
