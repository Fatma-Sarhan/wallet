<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Process;
use App\User;
use Auth;
use Mail;
use App\Deposit;
use App\Withdraw;
use Carbon\Carbon;
use DB;

class UserController extends Controller
{
    public function deposit()
    {
    	return view('users.deposit');
    }

    public function newdeposit(Request $request)
    {
    	$id = Auth::user()->id;
    	$user = User::findOrfail($id);
    	$old_blanace = $user->balance; 
    	$amount= $request->amount ;
    	if($amount <= 0)
    	{
    		$error = "You should deposit 0LE or greater.... ";
    		return Response::json(['st'=>'not','message'=>$error]);
    	}
    	else
    	{
    		//Update user...
	    	$new_blance = $old_blanace + $amount;
	    	$user->balance = $new_blance ;
	    	$user->update();
	    	//New deposit...
	    	$time =  Carbon::now();
	    	Deposit::create([
	            'amount' => $amount,
	            'created_at' =>$time,
	            'user_id' => $id,
	        ]);
	    	
	    	return Response::json($new_blance);
	    	}	

    }

    public function withdraw()
    {
    	return view('users.withdraw');
    }

    public function newwithdraw(Request $request)
    {
    	$id = Auth::user()->id;
    	$user = User::findOrfail($id);
    	$old_blanace = $user->balance; 
    	$amount= $request->amount ;
    	if($amount <= 0 || $amount > $old_blanace)
    	{
    		$error = "Not allowed value .... ";
    		return Response::json(['st'=>'not','message'=>$error]);
    	}
    	else
    	{
    		//Update user...
	    	$new_blance = $old_blanace - $amount;
	    	$user->balance = $new_blance ;
	    	$user->update();
	    	//New withdraw...
	    	$time =  Carbon::now();
	    	Withdraw::create([
	            'amount' => $amount,
	            'created_at' =>$time,
	            'user_id' => $id,
	        ]);

	        //Response for Ajax function...
	    	return Response::json($new_blance);
	    	}	
    
    }
    public function history()
    {
    	$user =Auth::user();
    	//Using relations (1:M) between models...
    	$deposits = $user->deposits;
    	$withdraws = $user->withdraws;
    	$transactions = $user->transactions;
    	return view('history',compact('deposits','withdraws','transactions'));
    	
    }

}
