<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Transaction extends Model
{
    protected $fillable = [ 'amount', 'user_id','to'];

    /* 1:M Relation user:transactions */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

