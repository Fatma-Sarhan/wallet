<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Deposit extends Model
{
    protected $fillable = [ 'amount', 'user_id'];

    /* 1:M Relation user:deposits */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
