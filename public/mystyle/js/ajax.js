$(function () {
	console.log('ajax here');

	//New Deposit....
    $("body").delegate('.deposit','click',function(e){
                console.log("ajaxdeposit");
                e.preventDefault();
                var _token=$(this).data('rowtok');
               	var amount =  $(this).parent().parent().find("#amount").val();
                
                var data ={};
                
                data._token=_token;
                data.amount=amount;
                console.log(data);

                $.ajax({
                    url:'/deposit',
                    type:'post',
                    data:data,
                    success:function(data)
                    {
                    	if(data.st != 'not')
                    	{
                    		console.log(data);
	                        document.getElementById('balance').innerHTML=data;
	                        $('#msg').removeClass('hide');
	                        $('#msg').removeClass('alert alert-danger');
	                        $('#msg').addClass('alert alert-success');
	                        document.getElementById('msg').innerHTML='<span>Deposit process done  successfully ....</span>';
	                        $("#msg").show().delay(3000).fadeOut();
                    	}
                    	else
                    	{
                    		console.log(data);
                			$('#msg').removeClass('hide');
                    		$('#msg').removeClass('alert alert-sucess');
	                        $('#msg').addClass('alert alert-danger');
	                        document.getElementById('msg').innerHTML='<span>'+data.message+'</span>';
	                        $("#msg").show().delay(5000).fadeOut();

                    	}
                    	
                 		
                    	
                    }         
                    
                 });
            });

    //New Withdraw....
    $("body").delegate('.withdraw','click',function(e){
                console.log("ajaxwithdraw");
                e.preventDefault();
                var _token=$(this).data('rowtok');
               	var amount =  $(this).parent().parent().find("#amount").val();
                
                var data ={};
                
                data._token=_token;
                data.amount=amount;
                console.log(data);

                $.ajax({
                    url:'/withdraw',
                    type:'post',
                    data:data,
                    success:function(data)
                    {
                    	if(data.st != 'not')
                    	{
		                	console.log(data);
		                    document.getElementById('balance').innerHTML=data;
		                    $('#msg').removeClass('hide');
		                    $('#msg').removeClass('alert alert-danger');
		                    $('#msg').addClass('alert alert-success');
		                    document.getElementById('msg').innerHTML='<span>Withdraw process done  successfully ....</span>';
		                    $("#msg").show().delay(3000).fadeOut();
		                }
                        else
                        {
                        	console.log(data);
                			$('#msg').removeClass('hide');
                    		$('#msg').removeClass('alert alert-sucess');
	                        $('#msg').addClass('alert alert-danger');
	                        document.getElementById('msg').innerHTML='<span>'+data.message+'</span>';
	                        $("#msg").show().delay(5000).fadeOut();

                        }
                    }         
                    
                 });
            });
	//New Transfer....
    $("body").delegate('.transfer','click',function(e){
                console.log("ajaxtransfer");
                e.preventDefault();
                var _token=$(this).data('rowtok');
               	var amount =  $(this).parent().parent().find("#amount").val();
                var email = $(this).parent().parent().find("#email").val();
                var data ={};
                
                data._token=_token;
                data.amount=amount;
                data.email = email;
                console.log(data);

                $.ajax({
                    url:'/transfer',
                    type:'post',
                    data:data,
                    success:function(data)
                    {
                    	switch(data.st)
                    	{
                    		case "true":
                    			console.log(data);
		                        document.getElementById('balance').innerHTML=data.amount;
		                        $('#msg').removeClass('hide');
		                        $('#msg').removeClass('alert alert-danger');
		                        $('#msg').addClass('alert alert-success');
		                        document.getElementById('msg').innerHTML='<span>Transfer completed ....</span>';
		                        $("#msg").show().delay(3000).fadeOut();
                    		break;

                    		case "false":
                    			console.log(data);
                    			$('#msg').removeClass('hide');
	                    		$('#msg').removeClass('alert alert-sucess');
		                        $('#msg').addClass('alert alert-danger');
		                        document.getElementById('msg').innerHTML='<span>'+data.message+'</span>';
		                        $("#msg").show().delay(5000).fadeOut();
                    		break;

                    		case "not":
                    			console.log(data);
	                    		$('#msg').removeClass('hide');
	                    		$('#msg').removeClass('alert alert-sucess');
		                        $('#msg').addClass('alert alert-danger');
		                        document.getElementById('msg').innerHTML='<span>'+data.message+'</span>';
		                        $("#msg").show().delay(5000).fadeOut();
                    		break;

                    		case "novalue":
                    			console.log(data);
	                    		$('#msg').removeClass('hide');
	                    		$('#msg').removeClass('alert alert-sucess');
		                        $('#msg').addClass('alert alert-danger');
		                        document.getElementById('msg').innerHTML='<span>'+data.message+'</span>';
		                        $("#msg").show().delay(5000).fadeOut();
                    		break;

                    	}	
                    	
                    }         
                    
                 });
            });
})