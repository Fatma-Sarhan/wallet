<?php $__env->startSection('content'); ?>
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   <h4> Welcome <?php echo e(Auth::user()->name); ?>  </h4>

                   <h5> your current balance at your wallet = <?php echo e(Auth::user()->balance); ?> LE</h5> 
                   <br>
                   <h5>Which process you want to do ? </h5>
                   <ul>
                        <li><a href="/deposit" > Deposit </a> </li>
                        <li><a href="/withdraw" > Withdraw </a> </li>
                        <li><a href="/transfer" > Transfer </a> </li>
                   </ul>
                    <div class="history">
                        <a href="/history" >YourHistory</a>
                    </div>
                     <div class="api">
                        <a href="/api/listuser?api_token=<?php echo e(Auth::user()->api_token); ?>">Test Api function </a>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>