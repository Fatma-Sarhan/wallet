<?php $__env->startSection('content'); ?>
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your History</div>

                <div class="panel-body">
                  <?php if(count($deposits) > 0): ?>
                  <h4> Your deposits</h4>
                  <?php $__currentLoopData = $deposits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deposit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <table class="table"> 
                      <tbody>
                        <tr><td>You deposit <span class="amount"><?php echo e($deposit->amount); ?></span> LE at <?php echo e($deposit->created_at->toDayDateTimeString()); ?></td><td></td></tr>
                        </tbody>
                    </table>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?>
                    <h4> No deposits yet ...</h4>
                  <?php endif; ?>

                   <?php if(count($withdraws) > 0): ?>
                   <h4> Your withdraws</h4>
                    <?php $__currentLoopData = $withdraws; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $withdraw): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <table class="table"> 
                        <tbody>
                          <tr><td>You withdraw <span class="amount"><?php echo e($withdraw->amount); ?></span> LE at <?php echo e($withdraw->created_at->toDayDateTimeString()); ?></td><td></td></tr>
                          </tbody>
                      </table>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                      <h4> No withdraws yet ...</h4>
                    <?php endif; ?>

                    <?php if(count($transactions) > 0): ?>
                   <h4> Your transactions</h4>
                    <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <table class="table"> 
                        <tbody>
                          <tr><td>You transfer <span class="amount"><?php echo e($transaction->amount); ?></span> LE at <?php echo e($transaction->created_at->toDayDateTimeString()); ?> to <span class="amount"><?php echo e($transaction->to); ?></span></td></tr>
                          </tbody>
                      </table>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                      <h4> No transactions yet ...</h4>
                    <?php endif; ?>

                    
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>