<?php $__env->startSection('content'); ?>

        <div class="flex-center position-ref full-height">
            <?php if(Route::has('login')): ?>
                <div class="top-right links">
                    <?php if(Auth::check()): ?>
                        <a href="<?php echo e(url('/home')); ?>">Home</a>
                    <?php else: ?>
                        <a href="<?php echo e(url('/login')); ?>">Login</a>
                        <a href="<?php echo e(url('/register')); ?>">Register</a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>          
        </div>
         <div class="intro-header">
        <div class="container new">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>My Wallet</h1>
                        <h3></h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                             <!-- Authentication Links -->
                        
                            <li><a href="<?php echo e(route('login')); ?>" class="btn btn-default btn-lg"><i class="fa fa-sign-in"></i> Login </a></li>
                            <li><a href="<?php echo e(route('register')); ?>" class="btn btn-default btn-lg" ><i class="fa fa-lock"></i> Register </a></li>
                        
                                    
                                </ul>
                            </li>
                        
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>