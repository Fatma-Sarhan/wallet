<?php $__env->startSection('content'); ?>
<div class="container new">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Deposit process</div>

                <div class="panel-body">
                  <h5> your current balance at your wallet = <span class="amount" id="balance"><?php echo e(Auth::user()->balance); ?>  </span>   LE</h5> 
                  <p>New deposit :</p>
    
                      <form class="form-inline" action="/deposit" method="POST">
                      <?php echo e(csrf_field()); ?>

                      <input type="number" class="form-control mb-2 mr-sm-2 mb-sm-0"  placeholder="Enter money amount" name="amount" id="amount" min="10" max="">
                       
                      <button type="submit" class="btn btn-primary deposit" data-rowtok="<?php echo e(csrf_token()); ?>">Deposit</button>
                    </form>
                    <br>
                    <div id="msg" class="hide"></div>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>